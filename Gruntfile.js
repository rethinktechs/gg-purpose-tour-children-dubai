module.exports = function(grunt){
    
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        dirs: {
            jsSrc: 'assets/js',
            jsDest: 'build/js',
            sassSrc: 'assets/scss',
            sassDest: 'build/css'
        },
        watch: {
            js: {
                files: ['<%= dirs.jsSrc %>/*.js'],
                tasks: ['uglify:js', 'concat:js'],
                options: {
                    
                }
            },
            css: {
                files: ['<%= dirs.sassSrc %>/*.scss'],
                tasks: ['sass', 'concat:css']
            }
        },        
        uglify: {
            options: {
                compress: true,
                mangle: true,       //To simplify the var
                compress: true,     // To remove white spaces
                sourceMap: false,
                sourceMapName: "build/js/application.map",
                banner: "/* \n\t @author: Paul Gardson Lesondra \n\t @email: hellopaulg@gmail.com \n\t @website:  \n\t @copyright: 2017  \n\t @last-update: <%= grunt.template.today('dd-mm-yyyy') %> \n*/\n"
            },
            js: {
                src: "<%= dirs.jsSrc %>/*.js",
                dest: "<%= dirs.jsDest %>/main.min.js"
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            target: {
                src: '<%= dirs.jsSrc %>/*.js'
            }
        },
        concat: {
            options: {
                separator: ';\n'
            },
            js: {
                src: ['<%= dirs.jsSrc %>/*.js'],
                dest: '<%= dirs.jsDest %>/main.js'
            },
            css: {
                src: ['assets/css/style.css'],
                dest: 'build/css/main.css'
            }
        },
        sass: {                                 
            dist: {                             
                options: {                      
                    style: 'compressed',
                    sourcemap: 'none'
                },
                files: {                         
                    'assets/css/style.css': 'assets/scss/style.scss',       // 'destination': 'source' 
                    //'assets/css/responsive.css': 'assets/scss/responsive.scss'
                }
            }
        },
        
        
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat')
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('clean', 'clean files', ['']);
}